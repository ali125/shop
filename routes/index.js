const express = require('express');
const cartsRouter = require('./carts');
const ordersRouter = require('./orders');
const locationsRouter = require('./location');
const auth = require('../middleware/auth');
const viewRouter = express.Router();
const apiRouter = express.Router();

/* GET home page. */
viewRouter.get('/', (req, res, next) => {
  res.render('web/index', { title: 'Express' });
});

// viewRouter.get('/test-email', (req, res, next) => {
//   sendMail({
//     to: 'ali.mortazavi977@gmail.com',
//     subject: 'text again',
//     html: '<p>Only other test</p>',
//     text: 'Only other test'
//   }, (err, resp) => {
//     if (err) {
//       return res.send(err);
//     }
//     return res.send(resp);
//   });
// });

viewRouter.use('/carts', auth, cartsRouter.viewRouter);
apiRouter.use('/carts', auth, cartsRouter.apiRouter);

viewRouter.use('/orders', auth, ordersRouter.viewRouter);
apiRouter.use('/orders', auth, ordersRouter.apiRouter);

viewRouter.use('/locations', locationsRouter.viewRouter);
apiRouter.use('/locations', locationsRouter.apiRouter);


apiRouter.get('/test', (req, res, next) => {
  const services = Array.apply(null, new Array(3)).map((i, _) => {
    const originalPrice = Math.floor(Math.random() * 100);
    const discount =  +(Math.random() * 100).toFixed(0);
    const price = +(originalPrice - (originalPrice * discount) / 100).toFixed(0);
    return ({
        id: _,
        title: 'اکستنشن مو',
        rate: +(Math.random() * 10).toFixed(1),
        price: price * 1000,
        originalPrice: originalPrice * 1000,
        discount,
    })
});
const cards = Array.apply(null, new Array(10)).map((i, _) => ({
    id: _,
    // title: 'اکستنشن مو',
    title: 'سالن بهاری',
    rate: +(Math.random() * 10).toFixed(1),
    description: ' این یک نوشته آزمایشی است که به طراحان و برنامه نویسان کمک میکند تا این عزیزان با بهره گیری از این نوشته تستی و آزمایشی بتوانند نمونه تکمیل شده از پروژه و طرح خودشان را به کارفرما نمایش دهند، استفاده از این متن تستی می تواند سرعت پیشرفت پروژه را افزایش دهد، و طراحان به جای تایپ و نگارش متن می توانند تنها با یک کپی و پست این متن را در کادرهای مختلف جایگزین نمائید. این نوشته توسط سایت لورم ایپسوم فارسی نگاشته شده است.',
    image: 'https://dummyimage.com/300X200',
    price: Math.floor(Math.random() * 100) * 1000,
    // entity: 'بهاره محبی'
    services
}));

  res.json({ services, cards });
});

module.exports = {
  viewRouter,
  apiRouter
};


